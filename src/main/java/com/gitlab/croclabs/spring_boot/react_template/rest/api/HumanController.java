package com.gitlab.croclabs.spring_boot.react_template.rest.api;

import com.gitlab.croclabs.spring_boot.react_template.rest.Rest;
import org.springframework.web.bind.annotation.GetMapping;

@Rest("/human")
public class HumanController {
	@GetMapping
	public String get() {
		return "human";
	}
}
