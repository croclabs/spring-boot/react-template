package com.gitlab.croclabs.spring_boot.react_template.rest;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
public class CsrfHolder {
	CsrfToken token;

	public CsrfHolder() {
		super();
	}

	public CsrfToken getToken() {
		return token;
	}

	public void setToken(CsrfToken token) {
		this.token = token;
	}
}