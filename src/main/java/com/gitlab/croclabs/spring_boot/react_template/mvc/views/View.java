package com.gitlab.croclabs.spring_boot.react_template.mvc.views;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Controller
@RequestMapping
public @interface View {
	@AliasFor(annotation = RequestMapping.class)
	String[] value() default "";
}
