package com.gitlab.croclabs.spring_boot.react_template.mvc.views.options;

import com.gitlab.croclabs.spring_boot.react_template.mvc.views.IView;
import com.gitlab.croclabs.spring_boot.react_template.mvc.views.View;

@View("/options")
public class Options implements IView {
	@Override
	public void init() {

	}
}
