package com.gitlab.croclabs.spring_boot.react_template.mvc.views;

import org.springframework.web.bind.annotation.GetMapping;

public interface IView {
	void init();

	@GetMapping("/**")
	default String getIndex() {
		init();
		return "/index.html";
	}
}
