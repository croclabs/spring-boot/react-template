package com.gitlab.croclabs.spring_boot.react_template;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {

	@Bean
	public InMemoryUserDetailsManager userDetailsService() {
		UserDetails user1 = User.withUsername("user1")
				.password(passwordEncoder().encode("user1Pass"))
				.roles("USER", "API")
				.build();
		UserDetails user2 = User.withUsername("user2")
				.password(passwordEncoder().encode("user2Pass"))
				.roles("USER")
				.build();
		UserDetails admin = User.withUsername("admin")
				.password(passwordEncoder().encode("adminPass"))
				.roles("ADMIN")
				.build();
		return new InMemoryUserDetailsManager(user1, user2, admin);
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http
				.authorizeHttpRequests(conf -> conf
						// ! use POST /logout to clean csrf token in session!
						.requestMatchers(HttpMethod.GET, "/logout").denyAll()
						.requestMatchers("/api/**").hasRole("API")
						.requestMatchers("/ui/**").authenticated()
						.anyRequest().permitAll()
				)
				.sessionManagement(conf -> conf.sessionFixation().newSession())
				// if not present: back to login
				// .csrf(conf -> conf.ignoringRequestMatchers("/api/**"))
				.formLogin(conf -> conf
						.loginPage("/login")
						.failureUrl("/login?auth=false")
						.defaultSuccessUrl("/")
						.loginProcessingUrl("/login")
						.usernameParameter("username")
						.passwordParameter("password")
				)
				.logout(conf -> conf
						.logoutSuccessUrl("/#/login")
						.deleteCookies("JSESSIONID")
						.invalidateHttpSession(true)
				);

		return http.build();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
