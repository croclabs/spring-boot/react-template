package com.gitlab.croclabs.spring_boot.react_template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactTemplateApplication.class, args);
	}

}
