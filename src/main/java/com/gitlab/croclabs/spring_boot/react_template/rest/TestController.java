package com.gitlab.croclabs.spring_boot.react_template.rest;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
@Log4j2
public class TestController {
	@GetMapping
	public void get() {
		log.debug("GET");
	}

	@PostMapping
	public void post() {
		log.debug("POST");
	}

	@PutMapping
	public void put() {
		log.debug("PUT");
	}

	@DeleteMapping
	public void delete() {
		log.debug("DELETE");
	}
}
