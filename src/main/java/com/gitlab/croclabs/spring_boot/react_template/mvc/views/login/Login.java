package com.gitlab.croclabs.spring_boot.react_template.mvc.views.login;

import com.gitlab.croclabs.spring_boot.react_template.mvc.views.IView;
import com.gitlab.croclabs.spring_boot.react_template.mvc.views.View;

@View("/login")
public class Login implements IView {
	@Override
	public void init() {
		//
	}
}
