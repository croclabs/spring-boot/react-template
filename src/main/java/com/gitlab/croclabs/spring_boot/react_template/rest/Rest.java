package com.gitlab.croclabs.spring_boot.react_template.rest;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RestController
@RequestMapping
public @interface Rest {
	@AliasFor(annotation = RequestMapping.class)
	String[] value();

	@AliasFor(attribute = "value", annotation = RestController.class)
	String name() default "";
}
