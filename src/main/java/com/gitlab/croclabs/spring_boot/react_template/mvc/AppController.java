package com.gitlab.croclabs.spring_boot.react_template.mvc;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Log4j2
@RequiredArgsConstructor
public class AppController implements ErrorController {
	final UserDetailsService users;
	final HttpServletRequest req;
	final HttpServletResponse res;


	@GetMapping("/")
	public String getIndex() {
		return "index.html";
	}

	@GetMapping({"/error"})
	public String handleError() {
		return "redirect:/login";
	}

	@PostMapping({"/error"})
	public String handlePostError() {
		return "redirect:/login";
	}
}
