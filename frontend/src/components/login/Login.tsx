import User from '../../utils/User';
import './Login.sass';

export default function Login() {
	return (
		<div className="loginWrapper">
			<div className='loginForm'>
				<div>
					<input id="username" name="username" placeholder='Username'/>
				</div>
				<div>
					<input type="password" id="password" name="password" placeholder='Password'/>
				</div>
				<div>
					<button onClick={() => User.login()}>Login</button>
				</div>
			</div>
		</div>
	);
}