import axios from "axios";
import { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import User from "../../utils/User";


function AuthRoute() {
	const [authorized, setAuthorized] = useState<boolean>(User.isAuthorized());

	useEffect(() => {
		axios.get('/user/current')
			.then(res => {
				if (res.data) {
					User.current = res.data;
					setAuthorized(true);
				} else {
					User.current = null;
					setAuthorized(false)
					window.location.href = '/login';
				}
			})
	}, []);

	return (
		<>
			{authorized ? <Outlet/> : <></>}
		</>
	)
}

export default AuthRoute;