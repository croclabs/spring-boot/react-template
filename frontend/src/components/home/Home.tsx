import { useEffect } from "react";
import User from "../../utils/User";
import axios from "axios";

export default function Home() {
	useEffect(() => {
		axios.get('/test')
		.then(() => axios.post('/test')
			.then(() => axios.put('/test')
				.then(() => axios.delete('/test'))));
	});

	axios.post('/test')
	axios.put('/test')
	axios.delete('/test');

	return (
		<>
			<div>Home</div>
			<div><button onClick={() => User.logout()}>Logout</button></div>
		</>
	)
}