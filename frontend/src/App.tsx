import { BrowserRouter, HashRouter, Outlet, Route, Routes } from 'react-router-dom';
import './App.scss';
import Login from './components/login/Login';
import AuthRoute from './components/routing/AuthRoute';
import Home from './components/home/Home';

function App() {
	return (
		<BrowserRouter>
			<Routes>
				<Route path='login' element={<Login/>}/>

				<Route element={<AuthRoute/>}>
					<Route path='' element={<Home/>}/>
					<Route path='options' element={<div>options <Outlet/></div>}>
						<Route path='opt1' element={<div>opt1 <Outlet></Outlet></div>}>
							<Route path='opt2' element={<div>opt2</div>}></Route>
						</Route>
					</Route>
				</Route>
			</Routes>
		</BrowserRouter>
	);
}

export default App;
