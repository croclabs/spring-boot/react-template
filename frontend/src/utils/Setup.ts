import ax from "axios";
import Csrf from "./Csrf";

export default class Setup {
	static axios() {
		ax.interceptors.request.use(async config => {
			let method = config.method ?? 'GET'

			if (method.toUpperCase() === 'GET') {
				return config;
			}

			let csrf = await Csrf.get();
			config.headers.set(csrf.headerName, csrf.token);

			return config;
		});
	}
}