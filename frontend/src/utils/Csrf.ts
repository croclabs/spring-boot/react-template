import axios from "axios";

export interface CsrfToken {
	headerName: string;
	token: string;
	parameterName: string;
}

export default class Csrf {
	static async reload(): Promise<CsrfToken> {
		return axios.get('/csrf')
			.then(res => {
				sessionStorage.setItem("_csrf", JSON.stringify(res.data));
				return res.data;
			});
	}

	static async get(): Promise<CsrfToken> {
		let csrfStr = sessionStorage.getItem("_csrf");

		if (csrfStr != null) {
			return JSON.parse(csrfStr);
		} else {
			return Csrf.reload();
		}
	}
}