import axios from "axios";
import Csrf from "./Csrf";

export default class User {
	static current: any;

	static isAuthorized(): boolean {
		return User.current != null;
	}

	static async logout() {
		await axios.post('/logout').then(res => {
			axios.get('/user/current')
				.then(res => {
					if (!res.data) {
						Csrf.reload();
						this.current = null;
						window.location.href = "/login?logout=true";
					}
				})
		});
	}

	static async login() {
		let username = document.getElementById('username') as HTMLInputElement;
		let password = document.getElementById('password') as HTMLInputElement;

		await Csrf.get().then(csrf => axios.post('/login', {username: username.value, password: password.value, _csrf: csrf.token}, {headers: {
			"Content-Type": "application/x-www-form-urlencoded"
		}}).then(res => {
			axios.get('/user/current')
				.then(res => {
					if (res.data) {
						Csrf.reload();
						this.current = res.data;
						window.location.href = "/";
					} else {
						window.location.href = "/login?auth=false";
					}
				})
		}));
	}
}